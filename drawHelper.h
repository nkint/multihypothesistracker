#ifndef DRAWHELPER_H
#define DRAWHELPER_H

#include <QColor>
#include <QList>

//------------------------------------------------ color function

static inline
void HSVtoRGB( float *r, float *g, float *b, float h, float s, float v )
{
    int i;
    float f, p, q, t;
    if( s == 0 ) {
        // achromatic (grey)
        *r = *g = *b = v;
        return;
    }
    h /= 60;			// sector 0 to 5
    i = floor( h );
    f = h - i;			// factorial part of h
    p = v * ( 1 - s );
    q = v * ( 1 - s * f );
    t = v * ( 1 - s * ( 1 - f ) );
    switch( i ) {
        case 0:
            *r = v;
            *g = t;
            *b = p;
            break;
        case 1:
            *r = q;
            *g = v;
            *b = p;
            break;
        case 2:
            *r = p;
            *g = v;
            *b = t;
            break;
        case 3:
            *r = p;
            *g = q;
            *b = v;
            break;
        case 4:
            *r = t;
            *g = p;
            *b = v;
            break;
        default:		// case 5:
            *r = v;
            *g = p;
            *b = q;
            break;
    }
}

//------------------------------------------------ YELLOW
static inline
QList<QColor> getYellows() {

    QColor yellow1( 248, 131, 121);
    QColor yellow2( 255, 250, 205);
    QColor yellow3( 255, 0, 0    );
    QColor yellow4( 255, 165, 0  );
    QColor yellow5( 255, 239, 0  );
    QColor yellow6( 196, 2, 51   );
    QColor yellow7( 255, 229, 180);
    QColor yellow8( 255, 88, 0   );
    QColor yellow9( 255, 211, 0  );
    QColor yellow10( 242, 0, 60   );
    QColor yellow11( 255, 153, 102);
    QColor yellow12( 239, 204, 0  );
    QColor yellow13( 237, 41, 57  );
    QColor yellow14( 255, 88, 0   );
    QColor yellow15( 255, 255, 120);
    QColor yellow16( 237, 145, 33 );
    QColor yellow17( 234, 60, 83  );
    QColor yellow18( 232, 97, 0   );

    QList<QColor> retval;
    retval.push_back( yellow1 );
    retval.push_back( yellow2 );
    retval.push_back( yellow3 );
    retval.push_back( yellow4 );
    retval.push_back( yellow5 );
    retval.push_back( yellow6 );
    retval.push_back( yellow7 );
    retval.push_back( yellow8 );
    retval.push_back( yellow9 );
    retval.push_back( yellow10 );
    retval.push_back( yellow11 );
    retval.push_back( yellow12 );
    retval.push_back( yellow13 );
    retval.push_back( yellow14 );
    retval.push_back( yellow15 );
    retval.push_back( yellow16 );
    retval.push_back( yellow17 );
    retval.push_back( yellow18 );

    return retval;
}

#endif // DRAWHELPER_H
