#ifndef MAINGUI_H
#define MAINGUI_H

#include <QWidget>
#include <QImage>
#include <opencv2/core/core.hpp>
#include "maincomponent.h"

namespace Ui {
class MainGUI;
}

class MainGUI : public MainComponent
{
    Q_OBJECT
    
public:
    explicit MainGUI(QWidget *parent = 0);
    virtual ~MainGUI();
    
private:
    Ui::MainGUI *ui;

    OpenCV_GLWidget *producerLabel;
    OpenCV_GLWidget *consumer1Label;

    void initGUI();

private slots:
    void showProducer(cv::Mat cvFrame);
    void showConsumer1(cv::Mat cvFrame);
    void showFPS(double n);
};

#endif // MAINGUI_H
