#include "producer.h"

#define ABORT_POLICY_LIMIT 2

Producer::Producer(Resources *r, std::string filename, QObject *parent):
    QThread(parent),
    r(r),
    frameCount(0),
    abort(false),
    pause(false),
    emptyFrameCounter(0),
    fpsDelay(100)
{
    qDebug() << "producer > ctor";

    videoMutex.lock();
    video = cv::VideoCapture(filename);
    videoMutex.unlock();
}

Producer::~Producer() {
    qDebug() << "Producer dtor";
    stopProduce();

    videoMutex.lock();
    if(video.isOpened()) {
        video.release();
    }
    videoMutex.unlock();

    wait();
}

void Producer::stopProduce() {
    abortMutex.lock();
    abort = true;
    abortMutex.unlock();
}

void Producer::togglePause()
{
    abortMutex.lock();
    if(pause) pause = false;
    else pause = true;
    abortMutex.unlock();
}

void Producer::run()
{
    forever {
        //-------------------------------- check for abort
        abortMutex.lock();
        if(abort) {
            abortMutex.unlock();
            break;
        }
        if(pause) {
            abortMutex.unlock();
            continue;
        }
        abortMutex.unlock();
        //-------------------------------- check for abort

        //-------------------------------- produce
        frameCount++;

        if(THREAD_VERBOSE) qDebug() << "Producer > producing: " << frameCount;

        cv::Mat cvFrame;
        video >> cvFrame;

        if(cvFrame.empty())  {
            emptyFrameCounter++;
            if( emptyFrameCounter>ABORT_POLICY_LIMIT ) {
                qDebug() << "Producer::run() > the video seems finished";
                break;
            }
            qDebug() << "Producer::run() > empty frame";
            continue;
        }
        cv::pyrDown(cvFrame, cvFrame);

        // TODO: apply mask and not only draw a rect
        cv::Rect rect;
        rect.width = cvFrame.cols;
        rect.height = cvFrame.rows;
        cv::rectangle(cvFrame, rect, cv::Scalar(0,0,0), 220);
        //-------------------------------- produce

        //-------------------------------- write in resources
        r->lastFrame_read.lockForWrite();

        r->lastFrame = cvFrame.clone();
        r->lastFrameCount = frameCount;
        emit newFrame(r->lastFrame);

        r->waitCondition.wakeAll();
        r->lastFrame_read.unlock();
        //-------------------------------- write in resources

        fpsMutex.lock();
        msleep(fpsDelay);
        fpsMutex.unlock();
    }
    if(THREAD_VERBOSE) qDebug() << "Producer > emit finished()";
    emit finished();
}
