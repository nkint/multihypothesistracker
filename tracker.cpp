#include "tracker.h"

#include <iostream>
#include <sstream>
#include <sys/time.h>
#include "drawHelper.h"

#define PERFORM_DUMP true

Tracker::Tracker() : 
    startTrackingAfterFrame(5)
{
    reset();

    if(PERFORM_DUMP)dumper.beginSession();
}

Tracker::~Tracker()
{
    if(PERFORM_DUMP)dumper.endSession(trackerFrameNr);
}

void Tracker::updateImage(const cv::Mat &inputImage, const long cameraFrameNr, double &msecElapsed)
{
    timeval t1, t2;
    // start timer
    gettimeofday(&t1, NULL);

    // background subtraction
    backgroundSubtractor.updateBackground(inputImage);

    // start tracking after frame
    if(trackerFrameNr > startTrackingAfterFrame) {
        // extract observation
        blobExtractor.extractObservations(backgroundSubtractor.getForegroundMask());
        // match!
        blobMatcher.matchNN(inputImage, activeObjects, blobExtractor.getObservations());

        // debug..
        draw_debug();
    } else {
        std::cout << "not tracking yet.." << std::endl;
    }

    // and now dump..
    if(PERFORM_DUMP) {
        if(firstFrame) dumper.setSize(inputImage.cols, inputImage.rows);
        dumpDown();
    }

    // stop timer
    gettimeofday(&t2, NULL);
    // compute and print the elapsed time in millisec
    msecElapsed = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
    msecElapsed += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
    //std::cout << msecElapsed << " ms, " << 1000.0/msecElapsed << " fps." << std::endl;
    msecElapsed = 1000.0/msecElapsed;

    this->trackerFrameNr++;
    if(firstFrame) firstFrame = false;
}

const cv::Mat Tracker::getBackground()
{
    return backgroundSubtractor.getBackground();
}

const cv::Mat Tracker::getForeground()
{
    return backgroundSubtractor.getForeground();
}

const cv::Mat Tracker::getForegroundMask()
{
    return backgroundSubtractor.getForegroundMask();
}

const cv::Mat Tracker::getGray()
{
    return gray;
}

void Tracker::reset()
{
    firstFrame = true;
    trackerFrameNr = 0;
    activeObjects.clear();
    blobExtractor.reset();
    blobMatcher.reset();
}

void Tracker::draw_debug()
{
    gray = blobExtractor.drawObservations(backgroundSubtractor.getForegroundMask());
    for(int i=0; i<(int)activeObjects.size(); i++) {
        float r,g,b;
        float n = (float)i/10 * 360;
        HSVtoRGB(&r,&g,&b, n, 1, 1);
        cv::Point blobCenter = activeObjects[i].getLastCorrection();
        const cv::RotatedRect _circle = cv::RotatedRect(blobCenter, cv::Size2f(20, 20), 0);
        cv::ellipse(gray, _circle, cv::Scalar(0, 0, 255), -1);

        std::stringstream ss;
        ss << activeObjects[i].label;
        cv::putText(gray,
                    ss.str(),
                    activeObjects[i].getLastCorrection(),
                    cv::FONT_HERSHEY_PLAIN,
                    2,
                    cv::Scalar(255,255,255));
    }
}

void Tracker::dumpDown()
{
    dumper.addFrame(trackerFrameNr);
    for(int i=0; i<(int)activeObjects.size(); i++) {
        int frame = trackerFrameNr;
        int label = activeObjects[i].label;
        //int x = activeObjects[i].getLastObservation().centroid.x;
        //int y = activeObjects[i].getLastObservation().centroid.y;
        int x = activeObjects[i].getLastCorrection().x;
        int y = activeObjects[i].getLastCorrection().y;
        dumper.addObject(frame, label, x, y);
    }
}

void Tracker::setFilename(std::string filename)
{
    dumper.setFilename(filename);
}

