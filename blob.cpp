#include "blob.h"

#include <iostream>

Blob::Blob()
{
}

void Blob::print()
{
    std::cout << "blob."
              << " area:" << this->area
              << " contour points: " << this->contour.size() << std::endl;
}
