#ifndef BLOBEXTRACTOR_H
#define BLOBEXTRACTOR_H

#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/background_segm.hpp>
#include "blob.h"

class BlobExtractor
{
public:
    BlobExtractor();
    void extractObservations(const cv::Mat &img);
    void reset();

    // debug
    cv::Mat drawObservations(cv::Mat image);

    bool simplify;
    double thresholdValue;
    double minArea, maxArea;
    int blobMinNPoints;

    std::vector<Blob> getObservations();

protected:
    std::vector<Blob> observations;
    std::vector<std::vector<cv::Point> > contours;

};

#endif // BLOBEXTRACTOR_H
