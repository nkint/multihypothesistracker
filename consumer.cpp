#include "consumer.h"

Consumer::Consumer(int id, Resources *r, QObject *parent):
    QThread(parent),
    abort(false)
{
    this->r = r;
    this->id = id;
    tracker.setFilename(r->filename);
}

Consumer::~Consumer() {
    qDebug() << QString("Consumer::~Consumer() > %1 dtor").arg(id);
    stopConsume();
    wait();
}

void Consumer::stopConsume() {
    abortMutex.lock();
    abort = true;
    abortMutex.unlock();
}

void Consumer::consumeMessage(cv::Mat cvFrame, int frameCount)
{
    if(THREAD_VERBOSE) qDebug() << QString("Consumer::consumeMessage > #%1 consuming: %2").arg(id).arg(frameCount);

    cvFrame = cvFrame.clone();

    double msecElapsed = 0;

    trackerMutex.lock();
    tracker.updateImage(cvFrame, frameCount, msecElapsed);
    trackerMutex.unlock();

    emit newComputation(tracker.getGray()); // convenience test matrix
    emit timeMeasuremed(msecElapsed);
}

void Consumer::run()
{
    forever {
        //-------------------------------- acquire the new frame
        r->lastFrame_read.lockForRead();
        // if there is not any new frame..
        if(r->lastFrameCount==-1 || r->lastFrameCount <= lastFrameCount) {
            if(THREAD_VERBOSE) qDebug() << QString("Consumer::run() > Consumer %1: going to sleep").arg(id);
            r->waitCondition.wait(&r->lastFrame_read);
            if(THREAD_VERBOSE) qDebug() << QString("Consumer::run() > Consumer %1: awake").arg(id);
        }
        lastFrameCount = r->lastFrameCount;
        lastFrame = r->lastFrame;

        r->lastFrame_read.unlock();
        //-------------------------------- acquire the new frame

        //-------------------------------- do the computations
        consumeMessage(lastFrame, lastFrameCount);
        //-------------------------------- do the computations

        //-------------------------------- check for abort
        abortMutex.lock();
        if(abort) {
            abortMutex.unlock();
            if(THREAD_VERBOSE) qDebug() << QString("Consumer::run() > Consumer %1: abort..").arg(id);

            break;
        } abortMutex.unlock();
        //-------------------------------- check for abort
    }
    if(THREAD_VERBOSE) qDebug() << "Consumer > emit finished()";
    emit finished();
}
