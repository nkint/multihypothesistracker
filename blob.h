#ifndef BLOB_H
#define BLOB_H

#include <vector>
#include <opencv2/core/core.hpp>

class Blob
{
public:
    Blob();

    double area;
    std::vector<cv::Point> contour;
    cv::RotatedRect ellipse;
    cv::Point2d centroid;
    cv::Rect boundingBox;

    cv::MatND histogram;

    void print();
};

#endif // BLOB_H
