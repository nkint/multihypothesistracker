#ifndef CONSUMER_H
#define CONSUMER_H

#include <QDebug>
#include <QCoreApplication>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QReadWriteLock>
#include <QString>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "resources.h"
#include "tracker.h"

class Consumer : public QThread
{
    Q_OBJECT

public:
    Consumer(int id, Resources *r, QObject *parent=0);
    ~Consumer();

    void stopConsume();

protected:
    Resources *r;
    int id;

    void consumeMessage(cv::Mat cvFrame, int frameCount);
    cv::Mat lastFrame;
    int lastFrameCount;

    void run();

private:
    QMutex abortMutex;
    bool abort;

    QMutex trackerMutex;
    Tracker tracker;

signals:
    void newComputation(cv::Mat);
    void finished();
    void timeMeasuremed(double);

};

#endif // CONSUMER_H
