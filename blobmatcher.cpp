#include "blobmatcher.h"

#include <algorithm>
#include <climits>
#include <iostream>

BlobMatcher::BlobMatcher():
    tracksIndex(0),
    maximumDistance(40), //64),
    persistence(12)
{
}

/**
 *    Match the nearest neighbour. Each new observation is associated with the most near track.
 *    For now, euclidean distance between centroids is considered.
 **/
double BlobMatcher::obs2trackCentroidDistance(const Blob &o, Track &t)
{
    const cv::Point &a = o.centroid;
    const cv::Point &b = t.getFuturePrediction();
    double dx = a.x - b.x;
    double dy = a.y - b.y;
    double distance = sqrtf(dx * dx + dy * dy);

    return distance;
}

double BlobMatcher::obs2trackHistogramDistance(const Blob &o, Track &t)
{
    double distance = cv::compareHist(o.histogram,
                                      t.getHistogram(),
                                      CV_COMP_BHATTACHARYYA);
    return distance;
}

/**
 *  Nearest Neighbout matching with a
 *  Single Hypothesis: each new observation is associated with the best track
 */
void BlobMatcher::matchNN(const cv::Mat &inputImage, std::vector<Track> &tracks, std::vector<Blob> newObservations)
{
    std::cout << "----------------------------------------------------------------------------------" << std::endl;

    int num_observation = newObservations.size();
    std::vector<int> updated_tracks;

    for(int i=0; i<num_observation; i++) {
        double min_distance = DBL_MAX;
        int min_distance_index = -1;

        // if no tracks, add new one, the first one
        if(tracks.size()==0) {
            addTrack(newObservations[i], tracks, inputImage);
        }

        for(int j=0; j<(int)tracks.size(); j++) {

            // euclidean distance
            double distance = obs2trackCentroidDistance(newObservations[i], tracks[j]);

            if(distance < min_distance) {
                min_distance = distance;
                min_distance_index = j;
            }
        }

        // associate the data to an existing track
        // single hypothesis !!!
        if(min_distance < maximumDistance) {
            int obs_index = i;
            int track_index = min_distance_index;
            int associated_track_label = blob2trackAssociation(track_index, obs_index,
                                                               inputImage,
                                                               tracks, newObservations);
            updated_tracks.push_back(associated_track_label);
        }
        // or create a new one
        else {
            std::cout << "==== created new track because the distance is:" << min_distance << std::endl;
            Track t = addTrack(newObservations[i], tracks, inputImage);
            updated_tracks.push_back(t.label);
        }
    }

    // remove the non-updated tracks
    for(int l=0; l<(int)tracks.size(); l++) {
        std::vector<int>::iterator itr;
        itr = std::find(updated_tracks.begin(), updated_tracks.end(), tracks[l].label);
        if( itr==updated_tracks.end() ) {
            tracks[l].inactiveFrames++;
        }
    }
    for( std::vector<Track>::iterator itr = tracks.begin(); itr != tracks.end() ; ) { // thanks silvio!!
        if( itr->inactiveFrames > persistence ) {
            itr = tracks.erase(itr);
        } else {
            itr++;
        }
    }
}

int BlobMatcher::blob2trackAssociation(int track_index, int obs_index,
                                        const cv::Mat &inputImage,
                                        std::vector<Track> &tracks,
                                        std::vector<Blob> newObservations)
{
    calcBlobHistogram(newObservations[obs_index], inputImage);
    double histogram_distance =
            obs2trackHistogramDistance(newObservations[obs_index],
                                               tracks[track_index]);

    if(histogram_distance < 0.965) {
        tracks[track_index].addObservation(newObservations[obs_index]);
//            std::cout << "added observation["<< i << "] to track[ "<<tracks[min_distance_index].label<<" ]"
//                      << " width distance: " << min_distance << std::endl;
        tracks[track_index].kalmanCorrect(newObservations[obs_index].centroid.x,
                                                 newObservations[obs_index].centroid.y);

        return tracks[track_index].label;
    } else {
        std::cout << "___ created new track because the HISTOGRAM distance is:" << histogram_distance << std::endl;
        Track t = addTrack(newObservations[obs_index], tracks, inputImage);

        return t.label;
    }
}

void BlobMatcher::reset()
{
    tracksIndex = 0;
}


Track BlobMatcher::addTrack(Blob newObservation, std::vector<Track> &tracks, const cv::Mat &inputImage)
{
    Track t;
    tracksIndex++;
    t.label = tracksIndex;
    if(newObservation.histogram.empty()) {
        calcBlobHistogram(newObservation, inputImage);
    }
    t.addObservation(newObservation);
    tracks.push_back(t);

    return t;
}

void BlobMatcher::calcBlobHistogram(Blob &b, const cv::Mat &inputImage)
{
    cv::Rect bboxObservation = b.boundingBox;
    cv::Mat o_img = inputImage(bboxObservation);
    cv::Mat hist;
    cv::Mat hsv;
    cv::cvtColor(o_img, hsv, CV_BGR2HSV);
    cv::Mat images[1] = {hsv};
    int channels[] = {0, 1}; // the histogram from the 0-th and 1-st channels (hue, saturation)
    int hbins = 12, sbins = 12; // Quantize the hue to 30 levels and the saturation to 32 levels
    int histSize[] = {hbins, sbins};
    float hranges[] = { 0, 180 }; // hue varies from 0 to 179, see cvtColor
    float sranges[] = { 0, 256 }; // saturation varies from 0 (black-gray-white) to 255 (pure spectrum color)
    const float* ranges[] = { hranges, sranges };
    cv::calcHist(images, 1, channels, cv::Mat(),
                 hist, 2, histSize, ranges,
                 true, // the histogram is uniform
                 false);

    b.histogram = hist;
}
