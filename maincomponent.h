#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QDebug>
#include <QWidget>
#include <QMainWindow>
#include <QLabel>
#include <QCloseEvent>
#include <QKeyEvent>
#include <QVBoxLayout>
#include <QSlider>
#include "resources.h"
#include "producer.h"
#include "consumer.h"
#include "opencv_glwidget.h"

#define NUM_CONSUMERS 1

class MainComponent : public QWidget
{
    Q_OBJECT
    
public:
    //------------------------------------------------------------------ ctor dtor
    explicit MainComponent(QWidget *parent = 0) : QWidget(parent) {
        qDebug() << "MainComponent ctor";

        qRegisterMetaType<cv::Mat>("cv::Mat");
    }

    virtual ~MainComponent()
    {
        qDebug() << "MainComponent dtor";
    }

    //------------------------------------------------------------------ 1 prod, n cons

protected slots:
    void startAll(std::string filename)
    {
        r.filename = filename;
        initConsumers();
        sleep(1);
        initProducer(filename);
    }

    void initProducer(std::string filename)
    {
        producer = new Producer(&r, filename);
        connect(producer, SIGNAL(finished()),
                this, SLOT(deleteProducer()), Qt::UniqueConnection);

        producer->start();
    }

    void initConsumers()
    {
        consumerCounter = 0;

        consumer1 = new Consumer(1, &r);

        connect(consumer1, SIGNAL(finished()),
                this, SLOT(deleteConsumer()));
        connect(consumer1, SIGNAL(destroyed()),
                this, SLOT(incrementConsumerCounter()));

        consumer1->start();
    }

    void deleteProducer() {
        // this function shouldn't be called manually:
        // it is called from finished() signal
        if(THREAD_VERBOSE) qDebug() << "deleteProducer";
        stopAll(); //stopConsumers();
        producer->deleteLater();
        producer = NULL;
    }

    void deleteConsumer() {
        // this function shouldn't be called manually:
        // it is called from finished() signal
        if(THREAD_VERBOSE) qDebug() << "deleteConsumer";
        consumer1->deleteLater();
        consumer1 = NULL;
    }

    void stopAll()
    {
        if(THREAD_VERBOSE) qDebug() << "stopAll";
        stopProducer();
        stopConsumers();
        if(THREAD_VERBOSE) qDebug() << "wake all!";
        r.waitCondition.wakeAll();
    }

    void stopProducer() {
        if(producer!=NULL) {
            producer->stopProduce();
        }
    }

    void stopConsumers() {
        if(consumer1!=NULL) {
            if(THREAD_VERBOSE) qDebug() << "stopConsumers";
            consumer1->stopConsume();
        }
    }

    void incrementConsumerCounter() {
        if(THREAD_VERBOSE) qDebug() << "incrementConsumerCounter";
        consumerCounter++;
        if(consumerCounter==NUM_CONSUMERS) {
            if(THREAD_VERBOSE) qDebug() << "..... all consumers are deleted!";
            consumer1 = NULL;
        }
    }

    void pauseProducer() {
        qDebug() << "pauseProducer";
        producer->togglePause();
    }

protected:

    void keyPressEvent(QKeyEvent* event) {
        if(event->key() == Qt::Key_S) {
            qDebug() << "pressed s, stop all";
            stopAll();
        }
    }

    void closeEvent(QCloseEvent * event)
    {
        qDebug() << "MainComponent > closeEvent";
        Q_UNUSED(event);
        emit shutdown();

        this->close();

        stopAll();
        sleep(1);

        // this while is needed to keep the event loop running
        // otherweys the "emit finish()" in the thread will never be delivered to
        // the connected slots
        while(consumerCounter!=NUM_CONSUMERS) {

            sleep(1);
            r.waitCondition.wakeAll();

            if(!producer->isRunning() && !consumer1->isRunning()) {
                //deleteProducer();
                break;
            }

            if(producer->isRunning()) {
                qDebug() << "MainComonent dtor waiting for producer..";
            } if(consumer1->isRunning()) {
                qDebug() << "MainComponent dtor waiting for consumer..";
            }
        }

    }

protected:
    Resources r;
    Producer *producer;
    Consumer *consumer1;

    int consumerCounter;

signals:
    void shutdown();
};

#endif // MAINWIDGET_H
