#include "dumper.h"

#include <iostream>
#include <sstream>
#include <QDateTime>

Dumper::Dumper():
    QObject()
{
}

void Dumper::beginSession()
{
    // TODO: to be refactored for multiple sessions
    openFile();
    root.clear();
    sessions.clear();

    session.clear();
    session.insert("combinations", QVariant( QString("MOG + MHKF") ));
    frames.clear();
    frame.clear();
}

void Dumper::addFrame(int frameNr)
{
    if(!frame.empty()) {
        frame.insert("objects", objects);
        frames.append(frame);
    }
    objects.clear();
    frame.insert("frameNumber", QVariant(frameNr));
}

void Dumper::addObject(long frameNr, int label, int x, int y)
{
    // TODO: check on frame

    object.clear();

    object.insert("object id", QVariant(label));
    object.insert("x", QVariant(x));
    object.insert("y", QVariant(y));

    objects.append(object);
}

void Dumper::endSession(int frameNr)
{
    root.insert("overall", QVariant(frameNr));

    addFrame(frameNr); // be sure to add the last frame
    session.insert("frames", QVariant(frames));
    sessions.append(session);
    root.insert("sessions", QVariant(sessions));

    QJson::Serializer serializer;
    bool ok;
    QByteArray jsondata = serializer.serialize(root, &ok);
    if(!ok) {
        std::cerr << "ERROR > serializer error!!";
    }

    this->dumpfile.write(jsondata);

    closeFile();
}

void Dumper::setFilename(std::string filename)
{
    root.insert("source", QVariant( QString::fromStdString(filename) ));
}

void Dumper::setSize(int width, int height)
{
    root.insert("width", QVariant(width));
    root.insert("height", QVariant(height));
}

void Dumper::openFile()
{
    std::time_t t = std::time(0);
    std::stringstream ss;
    ss << "dump" << t << ".json";
    filename = QString(QString::fromStdString(ss.str()));

    // open file for writing
    dumpfile.setFileName( filename );
    if (!dumpfile.open(QIODevice::ReadWrite)) {
        std::cout << "ERROR: dumper > begin sessione > file not reachable" << std::endl;
        return;
    } else {
        std::cout << "file  "<< filename.toStdString() <<" correctly opened" << std::endl;
    }
}

void Dumper::closeFile()
{
    dumpfile.close();
    std::cout << "file  "<< filename.toStdString() <<" closed" << std::endl;
}


