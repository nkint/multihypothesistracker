#include "backgroundsubtractor.h"

BackgroundSubtractor::BackgroundSubtractor()
{
    bg_model.set("noiseSigma", 10);
}

void BackgroundSubtractor::updateBackground(const cv::Mat &img)
{
    if( fgimg.empty() )
        fgimg.create(img.size(), img.type());

    bg_model(img, fgmask, -1); //-1, update the model

    fgimg = cv::Scalar::all(0);
    img.copyTo(fgimg, fgmask);

    bg_model.getBackgroundImage(bgimg);
}

const cv::Mat BackgroundSubtractor::getBackground()
{
    return bgimg;
}

const cv::Mat BackgroundSubtractor::getForeground()
{
    return fgimg;
}

const cv::Mat BackgroundSubtractor::getForegroundMask()
{
    return fgmask;
}
