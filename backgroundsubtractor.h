#ifndef BACKGROUNDSUBTRACTOR_H
#define BACKGROUNDSUBTRACTOR_H

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/background_segm.hpp>

class BackgroundSubtractor
{
public:
    BackgroundSubtractor();

    void updateBackground(const cv::Mat &img);

    const cv::Mat getBackground();
    const cv::Mat getForeground();
    const cv::Mat getForegroundMask();

private:
    cv::BackgroundSubtractorMOG bg_model;
    cv::Mat fgmask, fgimg, bgimg;

};

#endif // BACKGROUNDSUBTRACTOR_H
