#include "track.h"

#include <iostream>

Track::Track():
    label(-1),
    inactiveFrames(0)
{
}

void Track::addObservation(Blob b)
{
    observations.push_back(b);

    if(observations.size() == 1) {
        kalmanInit(b.centroid.x, b.centroid.y);
        histogram = b.histogram;
    } else {
        // compute the moving average histogram
        float alpha = 0.9f;
        histogram = (1-alpha)*histogram + alpha*b.histogram;
    }
}

const Blob &Track::getLastObservation()
{
    return observations.at( observations.size()-1 );
}

void Track::kalmanInit(float x, float y) // initial position
{
    // Instantate Kalman Filter with
    // 4 dynamic parameters and 2 measurement parameters,
    // where my measurement is: 2D location of object,
    // and dynamic is: 2D location and 2D velocity.
    KF.init(4, 2, 0);

    measurement = cv::Mat_<float>::zeros(2,1);
    measurement.at<float>(0, 0) = x;
    measurement.at<float>(0, 0) = y;

    KF.statePre.setTo(0);
    KF.statePre.at<float>(0, 0) = x;
    KF.statePre.at<float>(1, 0) = y;

    KF.statePost.setTo(0);
    KF.statePost.at<float>(0, 0) = x;
    KF.statePost.at<float>(1, 0) = y;

    float k = 0.00005; //noise parameter

    setIdentity(KF.processNoiseCov, cv::Scalar::all(k)); //adjust this for faster convergence - but higher noise
    setIdentity(KF.measurementNoiseCov, cv::Scalar::all(1e-1));
    setIdentity(KF.errorCovPost, cv::Scalar::all(.1));

    // matrix values from here: http://www.mathworks.de/de/help/vision/ref/vision.kalmanfilterclass.html
    KF.transitionMatrix = *(cv::Mat_<float>(4, 4) << 1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1);
    KF.measurementMatrix = *(cv::Mat_<float>(2,4) << 1,0,0,0,   0,1,0,0);
}

cv::Point Track::kalmanCorrect(float x, float y)
{
    measurement(0) = x;
    measurement(1) = y;

    cv::Mat estimated = KF.correct(measurement);
    cv::Point statePt( estimated.at<float>(0),
                       estimated.at<float>(1) );

    corrections.push_back(statePt);

    return statePt;
}

cv::Point Track::kalmanPredict()
{
    cv::Mat prediction = KF.predict();
    cv::Point predictPt( prediction.at<float>(0),
                         prediction.at<float>(1) );
    return predictPt;
}

const cv::Point &Track::getLastCorrection()
{
    if(corrections.size()>0) {
        return corrections.at( corrections.size()-1 );
    }
    else {
        std::cout << "Track::getLastCorrection() > but no corrections yet" << std::endl;
        return getLastObservation().centroid;
    }
}

cv::Point Track::getFuturePrediction()
{
    cv::Point prediction = this->kalmanPredict();
    if(prediction == getLastCorrection()) {
        std::cout << "no." << std::endl;
    }
    return prediction;
}

const cv::MatND &Track::getHistogram()
{
    return this->histogram;
}
