#include <QApplication>
#include "maincomponent.h"
#include "maingui.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainGUI w;
    w.show();

    return a.exec();
}
