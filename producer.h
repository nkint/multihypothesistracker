#ifndef PRODUCER_H
#define PRODUCER_H

#include <string>
#include <QDebug>
#include <QCoreApplication>
#include <QThread>
#include <QMutex>
#include <QReadWriteLock>
#include <QWaitCondition>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "resources.h"

class Producer : public QThread
{
    Q_OBJECT

public:
    explicit Producer(Resources *r, std::string filename, QObject *parent = 0);
    ~Producer();

    void stopProduce();
    void togglePause();
    
protected:
    Resources *r;
    int frameCount;

    void run();

private:
    QMutex abortMutex;
    bool abort;
    bool pause;

    QMutex videoMutex;
    cv::VideoCapture video;
    int emptyFrameCounter;

    QMutex fpsMutex;
    int fpsDelay;

signals:
    void newFrame(cv::Mat);
    void finished();

public slots:
    //bool endCapture();

};

#endif // PRODUCER_H
