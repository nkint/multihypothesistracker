#-------------------------------------------------
#
# Project created by QtCreator 2013-08-03T20:13:28
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MultiHypothesisTracker
TEMPLATE = app

macx {
    QMAKE_CFLAGS_X86_64 += -mmacosx-version-min=10.7
    QMAKE_CXXFLAGS_X86_64 = $$QMAKE_CFLAGS_X86_64

    message("* Using settings for Mac OS X.")

    LIBS += -framework GLUT -framework OpenGL -framework Cocoa

    # WORKS ONLY IF THE PROJECT IS OPENED FROM THE TERMINAL!!!
    #LIBS += `pkg-config opencv --cflags --libs`
    INCLUDEPATH += /usr/local/include/opencv
    LIBS += -L/usr/local/lib/ \
        -lopencv_core \
        -lopencv_highgui \
        -lopencv_calib3d \
        -lopencv_imgproc \
        -lopencv_video
}

INCLUDEPATH += /usr/local/Cellar/qjson/0.8.1/include
LIBS += -L/usr/local/Cellar/qjson/0.8.1/lib -lqjson

SOURCES += main.cpp \
    producer.cpp \
    consumer.cpp \
    opencv_glwidget.cpp \
    tracker.cpp \
    blobextractor.cpp \
    blob.cpp \
    backgroundsubtractor.cpp \
    track.cpp \
    blobmatcher.cpp \
    dumper.cpp \
    maingui.cpp

HEADERS  += \
    producer.h \
    consumer.h \
    resources.h \
    opencv_glwidget.h \
    tracker.h \
    blobextractor.h \
    blob.h \
    backgroundsubtractor.h \
    track.h \
    blobmatcher.h \
    dumper.h \
    maingui.h \
    maincomponent.h \
    drawHelper.h

FORMS    += \
    maingui.ui
