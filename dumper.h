#ifndef DUMPER_H
#define DUMPER_H

#include <string>
#include <QObject>
#include <QFile>
#include <QVariantMap>
#include <QMap>
#include <QString>
#include <QVariantList>
#include <qjson/parser.h>
#include <qjson/serializer.h>


class Dumper : public QObject
{
    Q_OBJECT

public:
    Dumper();

    void beginSession();
    void addFrame(int frameNr);
    void addObject(long frameNr, int label, int x, int y);
    void endSession(int frameNr);
    void setFilename(std::string filename);
    void setSize(int width, int height);

private:
    void openFile();
    void closeFile();
    QString filename;
    QFile dumpfile;

    QMap<QString, QVariant> root;
    QVariantList sessions;
    QMap<QString, QVariant> session;
    QVariantList frames;
    QMap<QString, QVariant> frame;
    QVariantList objects;
    QMap<QString, QVariant> object;

};

#endif // DUMPER_H
