#ifndef TRACKER_H
#define TRACKER_H

#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "backgroundsubtractor.h"
#include "blobextractor.h"
#include "blobmatcher.h"
#include "dumper.h"

class Tracker
{   
public:
    Tracker();
    ~Tracker();

    void updateImage(const cv::Mat &inputImage,
                     const long int cameraFrameNr,
                     double &msecElapsed);
    void reset();

    // convenience delegation methods for background subtractor
    const cv::Mat getBackground();
    const cv::Mat getForeground();
    const cv::Mat getForegroundMask();

    // DEBUG
    const cv::Mat getGray(); // convenience function to send image to the gui
                             // (until a new gui is not done..)

    void draw_debug();
    void dumpDown();

    void setFilename(std::string filename);

private:
    int trackerFrameNr;
    bool firstFrame;
    BackgroundSubtractor backgroundSubtractor;
    BlobExtractor blobExtractor;
    BlobMatcher blobMatcher;
    std::vector<Track> activeObjects;

    // parameters..
    int startTrackingAfterFrame;

    Dumper dumper;

    // debug
    cv::Mat gray;
};

#endif // TRACKER_H
