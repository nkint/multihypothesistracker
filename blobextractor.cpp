#include "blobextractor.h"

#include <climits>
#include <limits>
#include <iostream>
#include "drawHelper.h"

BlobExtractor::BlobExtractor() :
    simplify(false),
    thresholdValue(28.),
    minArea(40),
    maxArea(-1),
    blobMinNPoints(4)
{
}

void BlobExtractor::extractObservations(const cv::Mat &foreground)
{
    observations.clear();
    contours.clear();
    cv::Mat thresh;
    bool do_threshold = false;

    // be sure we have 1 channel matrix
    if(foreground.channels() == 1) {
        thresh = foreground.clone();
        //std::cout << "blob extractor > image already 1 channel" << std::endl;
    } else if(foreground.channels() == 3) {
        cv::cvtColor(foreground, thresh, CV_RGB2GRAY);
        //std::cout << "blob extractor > image 3 channels, convert to gray" << std::endl;
        do_threshold = true;
    } else if(foreground.channels() == 4) {
        cv::cvtColor(foreground, thresh, CV_RGBA2GRAY);
        //std::cout << "blob extractor > image 4 channels, convert to gray" << std::endl;
        do_threshold = true;
    }

    // some pre-processing, should be moved to a class?
    int niters = 3;
    cv::Mat temp;
    cv::dilate(thresh, temp, cv::Mat(), cv::Point(-1,-1), niters);
    cv::erode(temp, temp, cv::Mat(), cv::Point(-1,-1), niters*2);
    cv::dilate(temp, thresh, cv::Mat(), cv::Point(-1,-1), niters);

    // theshold
    if(do_threshold) {
        cv::threshold(thresh, thresh, thresholdValue, 255, CV_8U);
    }

    // run the opencv contourn finder
    std::vector<std::vector<cv::Point> > allContours;
    int simplifyMode = simplify ? CV_CHAIN_APPROX_SIMPLE : CV_CHAIN_APPROX_NONE;
    cv::findContours(thresh, allContours, CV_RETR_EXTERNAL, simplifyMode);

    // filter and create blobs
    for(int i = 0; i < (int)allContours.size(); i++) {
        double area = cv::contourArea(allContours[i]);
        double max_area = (maxArea==-1) ? (foreground.rows*foreground.cols) : (maxArea);
        if( ( area >= minArea && area <= max_area ) &&
            ( (int)allContours[i].size() > blobMinNPoints )    ) {
            Blob new_observation;
            new_observation.area = area;
            new_observation.contour = allContours[i];
            new_observation.ellipse = cv::fitEllipse(allContours[i]);
            new_observation.centroid = new_observation.ellipse.center;
            new_observation.boundingBox = new_observation.ellipse.boundingRect();
            observations.push_back(new_observation);
            contours.push_back(allContours[i]);
        } else {
//            std::cout << " =================== blob extractor > discard blob "
//                      << area << " "
//                      << allContours[i].size() << std::endl;
        }
    }
}

void BlobExtractor::reset()
{
    observations.clear();
    contours.clear();
}

QList<QColor> yellows = getYellows();
cv::Mat BlobExtractor::drawObservations(cv::Mat image)
{
    cv::Mat out;

    if(image.channels()!=3) {
        cv::cvtColor( image, out, CV_GRAY2RGB );
    } else {
        out = image.clone();
    }

//    std::cout << "blob extractor > draw on an image of type: " << image.type() << " " <<  CV_8UC3
//              << " " << image.channels() << std::endl;
//    std::cout << "blob extractor > draw on an image of dimension: " << image.rows << " " <<  image.cols << std::endl;
//    cv::cvtColor( image, output, CV_GRAY2RGB );

    for(int i=0; i<(int)contours.size(); i++) {
        QColor y = yellows.at(i);
        cv::drawContours(out, contours, i, cv::Scalar(y.blue(), y.green(), y.red()), -1);
        //cv::drawContours(out, contours, i, cv::Scalar(255,0,0), -1);
        cv::ellipse(out, cv::fitEllipse(contours[i]), cv::Scalar(0,255,0), 2);
    }

//    if(contours.size()>0)
//        std::cout << "BlobExtractor::drawObservations > drawing: " << contours.size() << " blob!" << std::endl;

    return out;
}

std::vector<Blob> BlobExtractor::getObservations()
{
    return observations;
}
