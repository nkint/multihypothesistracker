#include "maingui.h"
#include "ui_maingui.h"

#include <QDebug>

MainGUI::MainGUI(QWidget *parent) :
    MainComponent(parent),
    ui(new Ui::MainGUI)
{
    ui->setupUi(this);

    qDebug() << "MainGUI ctor";

    startAll("/Users/alberto/Movies/dominiks/video_alberto/test_tanti1.mp4");
    initGUI();
}

MainGUI::~MainGUI()
{
    qDebug() << "MainGUI dtor";

    producerLabel->deleteLater();
    consumer1Label->deleteLater();

    delete ui;
}

void MainGUI::initGUI() {

    this->producerLabel = new OpenCV_GLWidget;
    producerLabel->setParent(ui->frameBackground);

    connect(producer, SIGNAL(newFrame(cv::Mat)),
            this, SLOT(showProducer(cv::Mat)));

    this->consumer1Label = new OpenCV_GLWidget;
    consumer1Label->setParent(ui->frameLabels);
    connect(consumer1, SIGNAL(newComputation(cv::Mat)),
            this, SLOT(showConsumer1(cv::Mat)));

    connect(consumer1, SIGNAL(timeMeasuremed(double)),
            this, SLOT(showFPS(double)));

    connect(ui->buttonPause, SIGNAL(clicked()),
            this, SLOT(pauseProducer()));
}

void MainGUI::showProducer(cv::Mat cvFrame) {
    QImage qtFrame(cvFrame.data, cvFrame.size().width, cvFrame.size().height, cvFrame.step, QImage::Format_RGB888);
    qtFrame = qtFrame.rgbSwapped();
    this->producerLabel->renderImage(qtFrame);
}

void MainGUI::showConsumer1(cv::Mat cvFrame) {
    QImage qtFrame(cvFrame.data, cvFrame.size().width, cvFrame.size().height, cvFrame.step, QImage::Format_RGB888);
    qtFrame = qtFrame.rgbSwapped();
    this->consumer1Label->renderImage(qtFrame);
}

void MainGUI::showFPS(double n)
{
    ui->fpsTrackerLabel->setText(QString( "FPS overall: " )+QString::number(n));
}
