#ifndef TRACK_H
#define TRACK_H

#include <vector>
#include <opencv2/video/tracking.hpp>
#include <opencv2/core/core.hpp>
#include "blob.h"

class Track
{
public:
    Track();

    void addObservation(Blob b);
    const Blob &getLastObservation();

    int label;
    int inactiveFrames;
    std::vector<Blob> observations;
    std::vector<cv::Point> corrections;

    cv::KalmanFilter KF;
    cv::Mat_<float> measurement;
    cv::Mat_<float> state; // (x, y, Vx, Vy)

    void kalmanInit(float x, float y);
    cv::Point kalmanCorrect(float x, float y);
    cv::Point kalmanPredict();

    const cv::Point &getLastCorrection();
    cv::Point getFuturePrediction();

    cv::MatND histogram;
    const cv::MatND &getHistogram();
};

#endif // TRACK_H
