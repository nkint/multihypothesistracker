#ifndef BLOBMATCHER_H
#define BLOBMATCHER_H

#include <vector>
#include "blob.h"
#include "track.h"

class BlobMatcher
{
public:
    BlobMatcher();

    void matchNN(const cv::Mat &inputImage, std::vector<Track> &track, std::vector<Blob> newObservations);
    void reset();

    int tracksIndex;

    // parameters
    double maximumDistance;
    int persistence; // how many frames an object can last without being seen until the tracker forgets about it

    int blob2trackAssociation(int track_index, int obs_index,
                               const cv::Mat &inputImage,
                               std::vector<Track> &tracks,
                               std::vector<Blob> newObservations);

protected:
    double obs2trackCentroidDistance(const Blob &o, Track &t);
    double obs2trackHistogramDistance(const Blob &o, Track &t);

    Track addTrack(Blob newObservation, std::vector<Track> &tracks, const cv::Mat &inputImage);
    void calcBlobHistogram(Blob &b, const cv::Mat &inputImage);
};

#endif // BLOBMATCHER_H
